function val = is_in_simplex(lambda)
%IS_IN_SIMPLEX Summary of this function goes here
%   Detailed explanation goes here
val = 0;
cond1 = sum(lambda<0) == 0; % positive
cond2 = abs(sum(lambda)-1) <= 1e-10; % sums to 1 up to numerical errors
if cond1 && cond2
    val = 1;
end
end

