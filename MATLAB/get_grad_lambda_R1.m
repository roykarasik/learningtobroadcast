function grad_lambda_R1 = get_grad_lambda_R1(s,lambda,g,P,sigma_mat)
%GET_GRAD_S_R1 Summary of this function goes here
%   Detailed explanation goes here

N = numel(g);

% sigma_mat = get_sigma_mat(s,g,a);
sum_sigma_mat = sum(sigma_mat,2); % Mx1 vector

grad_lambda_rho = get_grad_lambda_rho(s,lambda,P);% MxM matrix

% Derivatives file, eq. (4)
grad_lambda_R1 = 1/N*grad_lambda_rho*sum_sigma_mat;

end

