function sigma_mat = get_sigma_mat(s,g,a)
%SIGMA_MAT Summary of this function goes here
%   Detailed explanation goes here

% Make sure s is column and g is row
s = s(:);
g = g(:).';

% Apply sigmoid
s_cumsum = cumsum(s); 
x = g-s_cumsum; % MxN matrix
sigma_mat = sigmoid(x,a);


end

