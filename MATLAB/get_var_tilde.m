function var_tilde = get_var_tilde(s,lambda,g,P,a,beta)
%GET_VAR_TILDE Summary of this function goes here
%   Detailed explanation goes here

% parameters
epsilon = 1e-5;

R_tilde = get_R_tilde(s,lambda,g,P,a);
N = numel(R_tilde);
I = round(N*beta);
if I == 0
    var_tilde = R_tilde(1) + epsilon; % the epsilon is to guarantee non-zero gradients (with respect to s and lambda)
else
    var_tilde = R_tilde(I);
end

% % Sanity check
% Nr = get_Nr_gr(R_tilde,var_tilde,g);
% disp(num2str(Nr/N/beta));


end

