function [S,Lambda,R] = init_gradient_descent2(M,beta,g,P,a)
%INIT_GRADIENT_DESCENT Summary of this function goes here
%   Detailed explanation goes here

% Parameters 
num_init_points = 10;

S = zeros(M,num_init_points);
Lambda = zeros(M,num_init_points);
R = zeros(1,num_init_points);

for ind = 1:num_init_points
    s =  exprnd(1,M,1);
%     s = rand(M,1); s = s/sum(s)*(1+exprnd(1));
    lambda = flipud(cumsum(rand(M,1)));lambda = lambda/sum(lambda);
    S(:,ind) = s;
    Lambda(:,ind) = lambda;
    R(ind) = get_var_tilde(s,lambda,g,P,a,beta);
end


end

