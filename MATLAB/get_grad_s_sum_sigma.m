function grad_s_sum_sigma = get_grad_s_sum_sigma(sigma_mat,a)
%GET_GRAD_S_SUM_SIGMA Summary of this function goes here
%   Detailed explanation goes here
[M,N] = size(sigma_mat);
tmp = sigma_mat.*(1-sigma_mat);
tmp2 = -a*sum(tmp,2); % Mx1 vector
grad_s_sum_sigma = triu(repmat(tmp2.',M,1)); % MxM matrix, column m is the gradient for s^m

end

