function [G,mixture_means,mixture_vars] = GMM_channel(N,num_datasets)
%GMM_CHANNEL Summary of this function goes here
%   Detailed explanation goes here

% Number of Gaussians
K = 1;%10; %2;

% Init channel coefficients
h = zeros(N,num_datasets);

% Mixture probabilities
prob_mixture = 1/K*ones(K,1); %[0.1;0.9]
% Non uniform Example: prob_mixture = [0.01;0.01;0.01;0.01;0.01;0.01;0.01;0.01;0.01;1-0.09];
cdf_mixture = [0;cumsum(prob_mixture)/sum(prob_mixture)];

% Mixture variances
%mixture_vars = ones(K,1);
mixture_vars = 16*ones(K,1);

% Mixtures means
%mu = 5;
%mixture_means = exprnd(mu,G,1)+1i*exprnd(mu,G,1);
%mixture_means = logspace(0,0.5,K); % uniform for 0dB to 10dB (power), no reason to use complex mean
%mixture_means = 0;
%mixture_means = [10^(-5/20), 10^(5/20)];
mixture_means = sqrt(20);

% Draw kernel index (ala Inverse transform sampling)
u = rand(N,1);
kernel_inds = zeros(N,1);
for ind = 1:N
    I = find(cdf_mixture>=u(ind),1)-1; % minus 1 is due to the artifical "0" in cdf_mixture
    kernel_inds(ind) = I;
    h(ind,:) = mixture_means(I) + sqrt(mixture_vars(I)/2)*(randn(1,num_datasets)+1i*randn(1,num_datasets));
end

G = abs(h).^2;
G = sort(G,1); % for the derivatives of tilde{f}

end

