function R_tilde = get_R_tilde(s,lambda,g,P,a)
%GET_R_TILDE Summary of this function goes here
%   Detailed explanation goes here

rho = get_rho(s,lambda,P);
sigma_mat = get_sigma_mat(s,g,a);
R_tilde = sum(rho.*sigma_mat,1); % eq. (24)

end

