function [grad_s_f_tilde,grad_lambda_f_tilde,grad_r_f_tilde] = get_f_tilde_grads(s,lambda,r,beta,g,P,a)
%GET_R1_GRADS Summary of this function goes here
%   Detailed explanation goes here

N = numel(g);

R_tilde = get_R_tilde(s,lambda,g,P,a);
[Nr,gr] = get_Nr_gr(R_tilde,r,g);

if isempty(gr) % Nr=0
    grad_s_f_tilde = zeros(size(s));
    grad_lambda_f_tilde = zeros(size(s));
    grad_r_f_tilde = 1;
else

    rho = get_rho(s,lambda,P);
    sigma_mat = get_sigma_mat(s,gr,a);

    grad_s_f_tilde = Nr/N/beta*get_grad_s_R1(s,lambda,gr,P,a,rho,sigma_mat);
    grad_lambda_f_tilde = Nr/N/beta*get_grad_lambda_R1(s,lambda,gr,P,sigma_mat);
    grad_r_f_tilde = 1-Nr/N/beta;
end

end

