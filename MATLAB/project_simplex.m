function val = project_simplex(lambda)
%PROJ_SIMPLEX Summary of this function goes here
%   Detailed explanation goes here

lambda = lambda(:);

if is_in_simplex(lambda)
    val = lambda;
else
    if numel(lambda)>1
        val = ProbSimplexProj(lambda.'); % this function works on row vectors
        val = val(:);
    else
        val = 1;
    end
end

