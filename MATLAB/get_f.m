function f = get_f(s,lambda,r,beta,g,P)
%GET_F_TILDE Summary of this function goes here
%   Detailed explanation goes here

R = get_R(s,lambda,g,P);
diff_pos = project_positive(r-R);
f = r-1/beta*mean(diff_pos,1);


end

