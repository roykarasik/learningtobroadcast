function [f_tilde_best,cvar_vec_best,s_best,lambda_best,r_best] = optimized_empirical_cvar_beta(S0,Lambda0,R0,beta,g,P,a,T)
%OPTIMIZED_EMPIRICAL_CVAR_BETA Summary of this function goes here
%   Detailed explanation goes here

num_init_points = size(S0,2);

f_tilde_best = -1;%0;
for ind = 1:num_init_points
    s0 = S0(:,ind);
    lambda0 = Lambda0(:,ind);
    r0 = R0(ind);
    
    % Projected gradient descent with current init point
    [f_tilde,cvar_vec,s,lambda,r] = projected_gradient_descent_empirical_cvar_beta(s0,lambda0,r0,beta,g,P,a,T);
    
    % Save best
     if f_tilde>=f_tilde_best
        f_tilde_best = f_tilde;
        s_best = s;
        lambda_best = lambda;
        r_best = r;
        cvar_vec_best = cvar_vec;
    end
end


end

