function g_subset = get_subset_training_set(g,num_elements)
%GET_SUBSET_TRAINING_SET Summary of this function goes here
%   Detailed explanation goes here
N = size(g,1);
p = randperm(N);
g_subset = g(p(1:num_elements),:);
g_subset = sort(g_subset,1);

end

