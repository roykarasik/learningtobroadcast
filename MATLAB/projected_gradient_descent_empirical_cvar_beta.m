function [f_tilde_best,cvar_vec,s_best,lambda_best,r_best,s_mat,lambda_mat,r_vec] = projected_gradient_descent_empirical_cvar_beta(s0,lambda0,r0,beta,g,P,a,T)
%PROJECTED_GRADIENT_DESCENT_EMPIRICAL_CVAR_BETA Summary of this function goes here
%   Detailed explanation goes here

% learning rates
eta_s = 0.01;
eta_lambda = 0.01;
eta_r = 0.01;
min_eta = 0.001;
patience = 100;
factor_lr = 0.1;

% Flags
flag_print = 0;
iterations_per_print = 1;

% Init
s_current = s0;
lambda_current = lambda0;
r_current = r0;

cvar_vec = zeros(T,1);
s_mat = zeros(numel(s0),T);
lambda_mat = zeros(numel(lambda0),T);
r_vec = zeros(1,T);
saved_f_tilde = 0;
counter_no_improv = 0;
for ind = 1:T
    % Memory
    s_prev = s_current;
    lambda_prev = lambda_current;
    r_prev = r_current;
    
    % Step in the direction of the gradient
    [grad_s_f_tilde,grad_lambda_f_tilde,grad_r_f_tilde] = get_f_tilde_grads(s_prev,lambda_prev,r_prev,beta,g,P,a);
    s_current = project_positive(s_prev+eta_s*grad_s_f_tilde);
    lambda_current = project_simplex(lambda_prev+eta_lambda*grad_lambda_f_tilde);
    r_current = get_var_tilde(s_current,lambda_current,g,P,a,beta);
%     if numel(g)*beta > 1 % N*beta>1
%         r_current = get_var_tilde(s_current,lambda_current,g,P,a,beta);
%     else
%         r_current = project_positive(r_prev+eta_r*grad_r_f_tilde);
%     end
    
    
    
    % Evaluate cvar
    f_tilde = get_f_tilde(s_current,lambda_current,r_current,beta,g,P,a);
    
    % Save values to buffers
    cvar_vec(ind) = f_tilde;
    s_mat(:,ind) = s_current;
    lambda_mat(:,ind) = lambda_current;
    r_vec(ind) = r_current;
    
    % Check improvement
    if f_tilde>saved_f_tilde
        counter_no_improv = 0;
        saved_f_tilde = f_tilde;
    else
        counter_no_improv = counter_no_improv+1;
    end
    
    % Reduce learning rate if patience runs out (and learning rate does not
    % reach minimum value)
    cond_decrease_eta = (counter_no_improv>patience) && (eta_s>min_eta);
    if cond_decrease_eta
        eta_s = factor_lr*eta_s;
        eta_lambda = factor_lr*eta_lambda;
        eta_r = factor_lr*eta_r;
        counter_no_improv = 0;
        saved_f_tilde = f_tilde;
    end
    
    % Prints
    if flag_print && mod(ind,iterations_per_print)==0
        disp(['Iteration ',num2str(ind),'/',num2str(T),': CVaR = ',num2str(f_tilde),', Counter = ',num2str(counter_no_improv),', Eta = ',num2str(eta_s)]);
    end
end

% Save best
[f_tilde_best,I] = max(cvar_vec);
s_best = s_mat(:,I);
lambda_best = lambda_mat(:,I);
r_best = r_vec(I);

end

