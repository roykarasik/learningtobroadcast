close all;clearvars;clc;

% Gen log file based on system time
dateTimeNow = datetime('now');
dateTimeNow.Format = 'MM-dd-yyyy-HHmm';
sysTime = char(dateTimeNow);
logName = ['./Logs/log_',sysTime,'.log'];
workspaceName = ['./Workspace/workspace_',sysTime,'.mat'];

% Open Log
diary(logName);

% Parameters
P_dB = 20;
%N_vec = round(logspace(1,4,4));
%N_vec = [1 unique(round(logspace(1,3,10)))];
%N_vec = [10:10:100, 150, 200, 500];
%N_vec = [100:20:300];
%N_vec = 350:50:700;
N_vec = unique(round(logspace(0,3,50)));
a = 10;
M_vec = [2 1 6];
T = 1000;
beta_vec = [0.1 1];
num_datasets = 1e3;

%Write parameters to log file
disp('**********************');
disp('Parameters:')
disp('----------------');
disp(['P_dB = ',num2str(P_dB)]);
disp(['N_vec = ',num2str(N_vec)]);
disp(['a = ',num2str(a)]);
disp(['M_vec = ',num2str(M_vec)]);
disp(['T = ',num2str(T)]);
disp(['beta_vec = ',num2str(beta_vec)]);
disp(['num_datasets = ',num2str(num_datasets)]);
disp(' ');

% Constants
P = 10^(P_dB/10);

%% Draw largest datasets
[G,mixture_means,mixture_vars] = GMM_channel(max(N_vec),num_datasets);
G_montecarlo = GMM_channel(1e4,1e3);
g_montecarlo = sort(G_montecarlo(:));

%% Preparing training sets for each N
disp('Preparing training sets for each N');
tic;
G_cell = cell(numel(N_vec),1);
G_cell{end} = get_subset_training_set(G,N_vec(end));
for ind_N = numel(N_vec)-1:-1:1
    G_cell{ind_N} = get_subset_training_set(G_cell{ind_N+1},N_vec(ind_N)); % nested training sets
end
toc;

%% Search for good starting points
disp('Searching for good starting points');
tic;
start_points = cell(numel(M_vec),numel(beta_vec));
for ind_M = 1:numel(M_vec)
    M = M_vec(ind_M);
    for ind_beta = 1:numel(beta_vec)
        beta = beta_vec(ind_beta);
        start_points{ind_M,ind_beta} = get_good_starting_points(G,M,beta,P);
    end
end
toc;


%% Buffers to store results
cvar_stats = cell(numel(M_vec),numel(beta_vec));
cvar_tilde_stats = cell(numel(M_vec),numel(beta_vec));


%% Open parallel pool
parpool = gcp;

%% Main loop
for ind_M = 1:numel(M_vec)
    M = M_vec(ind_M);
    for ind_beta = 1:numel(beta_vec)
        beta = beta_vec(ind_beta);
        % init
        S0 = start_points{ind_M,ind_beta}{1};
        Lambda0 = start_points{ind_M,ind_beta}{2};
        cvar_mat = zeros(num_datasets,numel(N_vec));
        empirical_cvar_mat = zeros(num_datasets,numel(N_vec));
        for ind_N = 1:numel(N_vec) 
            N = N_vec(ind_N);
            G_n = G_cell{ind_N}; 
            disp(' ');
            disp(['Main loop: M = ',num2str(M),', beta = ',num2str(beta),', N = ',num2str(N)])
            % Run over datasets
            empirical_cvar_vec = zeros(num_datasets,1);
            cvar_vec = zeros(num_datasets,1);
            tic;
            %for ind_dataset = 1:num_datasets
            parfor ind_dataset = 1:num_datasets
                % Dataset
                g = G_n(:,ind_dataset);
                
                % Start with r0 that matches s0 and lambda0
                R0 = zeros(1,size(S0,2));
                for ind_r0 = 1:numel(R0)
                    s = S0(:,ind_r0);
                    lambda = Lambda0(:,ind_r0);
                    R0(ind_r0) = get_var_tilde(s,lambda,g,P,a,beta);
                end
                  
                % Gradient descent with multiple starting points
                [empirical_cvar,gradient_path,s_best,lambda_best,r_best] = optimized_empirical_cvar_beta(S0,Lambda0,R0,beta,g,P,a,T);
                empirical_cvar_mat(ind_dataset,ind_N) = empirical_cvar;
                
                cvar_mat(ind_dataset,ind_N) = get_cvar(s_best,lambda_best,beta,g_montecarlo,P);
            end
            disp(['Mean CVaR: ',num2str(mean(cvar_mat(:,ind_N)))]);
            disp(['Mean Empirical CVaR: ',num2str(mean(empirical_cvar_mat(:,ind_N)))]);
            save(workspaceName); % Save output
            toc;
        end
        cvar_stats{ind_M,ind_beta} = cvar_mat;
        cvar_tilde_stats{ind_M,ind_beta} = empirical_cvar_mat;
    end
end
save(workspaceName); % Save output

%% Close log
diary off;


