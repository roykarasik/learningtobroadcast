close all; clearvars; clc;

% Parameters
N_vec = [1e1, 1e2, 1e3]; % dataset size
P = 100; % TX power
res = 1e-6; % resolution to search for u0 and u1
num_datasets = 5000;
n = 5e4; % number of elements in numerical integration
max_bisection = 1e3;

% Vectors to hold results
expected_ach_rate = zeros(size(N_vec));

% Loop over N
for ind_N = 1:numel(N_vec)
    tic;
    N = N_vec(ind_N);
    disp([num2str(ind_N),'/',num2str(numel(N_vec)),': N = ',num2str(N)]);
    ach_rate_vec = zeros(num_datasets,1);
    
    % Loop over datasets
    for ind_data = 1:num_datasets
        % Dataset (Rayleigh fading)
        h = 1/sqrt(2)*(randn(N,1)+1i*randn(N,1));
        g = abs(h).^2;
        
        % Find u0
        lb = 0;
        ub = 10;
        Flag = 1;
        iter = 1;
        while Flag && iter<=max_bisection
            pts = (lb+ub)/2;
            p = ksdensity(g,pts,'Support','positive'); % pdf
            F = ksdensity(g,pts,'Function','cdf','Support','positive'); % cdf
            I = (1-F-pts*p)/(pts^2*p); % I(u)
            if abs(I-P)<res
                Flag = 0;
            else
                if I>P
                    lb = pts;
                else
                    ub = pts;
                end
            end
            iter = iter+1;
        end
        u0 = pts;
        
        % Find u1
        lb = 0;
        ub = 10;
        Flag = 1;
        iter = 1;
        while Flag && iter<=max_bisection
            pts = (lb+ub)/2;
            p = ksdensity(g,pts,'Support','positive'); % pdf
            F = ksdensity(g,pts,'Function','cdf','Support','positive'); % cdf
            I = (1-F-pts*p)/(pts^2*p); % I(u)
            if abs(I)<res
                Flag = 0;
            else
                if I>0
                    lb = pts;
                else
                    ub = pts;
                end
            end
            iter = iter+1;
        end
        u1 = pts;
        
        % Calculate ach rate
        u = linspace(u0,u1,n);
        p = ksdensity(g,u,'Support','positive'); % pdf
        F = ksdensity(g,u,'Function','cdf','Support','positive'); % cdf
        I = (1-F-u.*p)./(u.^2.*p); % I(u)
        du = u(2)-u(1);
        rho = -diff(I)/du;
        integrand = u(2:end).*rho./(1+u(2:end).*I(2:end));
        F_true = 1-exp(-u);
        R_ach_nats = sum((1-F_true(2:end)).*integrand)*du;
        R_ach = R_ach_nats/log(2);
        ach_rate_vec(ind_data) = R_ach;
    end
    expected_ach_rate(ind_N) = mean(ach_rate_vec);
    disp(['Expected rate = ',num2str(expected_ach_rate(ind_N))]);
    toc;
    disp(' ');
end
save 'kde_rayleigh_mirror.mat';


