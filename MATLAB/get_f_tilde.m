function f_tilde = get_f_tilde(s,lambda,r,beta,g,P,a)
%GET_F_TILDE Summary of this function goes here
%   Detailed explanation goes here

R_tilde = get_R_tilde(s,lambda,g,P,a);
tmp = project_positive(r-R_tilde);
f_tilde = r-1/beta*mean(tmp,2);


end

