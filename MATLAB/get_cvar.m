function f = get_cvar(s,lambda,beta,g,P)
%GET_F_TILDE Summary of this function goes here
%   Detailed explanation goes here

R = get_R(s,lambda,g,P);
N = numel(R);
% Maximum over r
I = round(N*beta);
if I == 0
    r = 0;
else
    r = R(I);
end

% CVaR = max_r {f}
diff_pos = project_positive(r-R);
f = r-1/beta*mean(diff_pos,1);


end

