function grad_s_R1 = get_grad_s_R1(s,lambda,g,P,a,rho,sigma_mat)
%GET_GRAD_S_R1 Summary of this function goes here
%   Detailed explanation goes here

N = numel(g);

% rho = get_rho(s,lambda,P);
% sigma_mat = get_sigma_mat(s,g,a);
sum_sigma_mat = sum(sigma_mat,2); % Mx1 vector

grad_s_sum_sigma = get_grad_s_sum_sigma(sigma_mat,a); % MxM matrix
grad_s_rho = get_grad_s_rho(s,lambda,P);% MxM matrix

% Derivatives file, eq. (1)
arg1 = 1/N*grad_s_rho*sum_sigma_mat;
arg2 = 1/N*grad_s_sum_sigma*rho;
grad_s_R1 = arg1+arg2;


end

