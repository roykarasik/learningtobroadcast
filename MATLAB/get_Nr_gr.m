function [Nr,gr] = get_Nr_gr(R_tilde,r,g)
%GET_NR_GR Summary of this function goes here
%   Detailed explanation goes here

Nr = find(r>=R_tilde,1,'last');
gr = g(1:Nr);

if isempty(Nr)
    Nr = 0;
end

end

