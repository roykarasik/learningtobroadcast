function grad_s_rho = get_grad_s_rho(s,lambda,P)
%GET_GRAD_S_RHO Summary of this function goes here
%   Detailed explanation goes here

% Verify column vec
s = s(:);
lambda = lambda(:);

% Constants
s_cumsum = cumsum(s); 
I_cumsum = 1-cumsum(lambda);
M = numel(s);

% Derivatives file, eq. (3)
denum1 = 1+s_cumsum*P.*(lambda+I_cumsum);
denum2 = 1+s_cumsum*P.*I_cumsum;
tmp = 1/log(2)*lambda*P./denum1./denum2; % Mx1 vector

grad_s_rho = triu(repmat(tmp.',M,1)); % MxM matrix, column m is the gradient for s^m

end

