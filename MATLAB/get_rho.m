function val = get_rho(s,lambda,P)
%R_ELL Summary of this function goes here
%   Detailed explanation goes here

% Verify column vec
s = s(:);
lambda = lambda(:);

% Constants
s_cumsum = cumsum(s); 
I_cumsum = 1-cumsum(lambda);


val = log2(1+s_cumsum.*lambda*P./(1+s_cumsum.*I_cumsum*P)); % vector for each value of m from 1 to M

end

