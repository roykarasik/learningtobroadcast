function [G_cell,mixture_means,mixture_vars] = GMM_channel2(N_vec,num_datasets)
%GMM_CHANNEL Summary of this function goes here
%   Detailed explanation goes here

% Number of Gaussians
K = 1;%10; %2;

% Init channel coefficients for largest dataset
N_max = max(N_vec);
h = zeros(N_max,num_datasets);

% Mixture probabilities
prob_mixture = 1/K*ones(K,1); %[0.1;0.9]
% Non uniform Example: prob_mixture = [0.01;0.01;0.01;0.01;0.01;0.01;0.01;0.01;0.01;1-0.09];
cdf_mixture = [0;cumsum(prob_mixture)/sum(prob_mixture)];

% Mixture variances
%mixture_vars = ones(K,1);
mixture_vars = 16*ones(K,1);

% Mixtures means
%mu = 5;
%mixture_means = exprnd(mu,G,1)+1i*exprnd(mu,G,1);
%mixture_means = logspace(0,0.5,K); % uniform for 0dB to 10dB (power), no reason to use complex mean
%mixture_means = 0;
%mixture_means = [10^(-5/20), 10^(5/20)];
mixture_means = sqrt(20);

% Draw kernel index (ala Inverse transform sampling)
u = rand(N_max,1);
kernel_inds = zeros(N_max,1);
for ind = 1:N_max
    I = find(cdf_mixture>=u(ind),1)-1; % minus 1 is due to the artifical "0" in cdf_mixture
    kernel_inds(ind) = I;
    h(ind,:) = mixture_means(I) + sqrt(mixture_vars(I)/2)*(randn(1,num_datasets)+1i*randn(1,num_datasets));
end

% Take subsets ofr each N
G_cell = cell(numel(N_vec),1);
for ind = 1:numel(N_vec)
    N = N_vec(ind);
    h_subset = h(1:N,:);
    G_subset = abs(h_subset).^2;
    G_subset = sort(G_subset,1); % for the derivatives of tilde{f}
    G_cell{ind} = G_subset;
end

end

