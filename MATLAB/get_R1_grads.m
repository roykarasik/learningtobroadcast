function [grad_s_R1,grad_lambda_R1] = get_R1_grads(s,lambda,g,P,a)
%GET_R1_GRADS Summary of this function goes here
%   Detailed explanation goes here

rho = get_rho(s,lambda,P);
sigma_mat = get_sigma_mat(s,g,a);

grad_s_R1 = get_grad_s_R1(s,lambda,g,P,a,rho,sigma_mat);
grad_lambda_R1 = get_grad_lambda_R1(s,lambda,g,P,sigma_mat);

end

