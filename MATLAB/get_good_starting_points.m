function start_points = get_good_starting_points(G,M,beta,P)
%GET_GOOD_STARTING_POINTS Summary of this function goes here
%   Detailed explanation goes here

num_start_points = 100;
num_best_start_points = 10;
num_datasets = size(G,2);

S = zeros(M,num_start_points);
Lambda = zeros(M,num_start_points);
mean_cvar_vec = zeros(num_start_points,1);
for ind_points = 1:num_start_points
    s = exprnd(1,M,1);
    S(:,ind_points) = s; 
    lambda = flipud(cumsum(rand(M,1)));lambda = lambda/sum(lambda);
    Lambda(:,ind_points) = lambda;
    cvar_vec = zeros(num_start_points,1);
    for ind_data = 1:num_datasets
        g = G(:,ind_data);
        cvar_vec(ind_data) = get_cvar(s,lambda,beta,g,P);
    end
    mean_cvar_vec(ind_points) = mean(cvar_vec);
end
[cvar_sorted,I] = sort(mean_cvar_vec,'descend');
start_points = {S(:,I(1:num_best_start_points)),Lambda(:,I(1:num_best_start_points))};




end

