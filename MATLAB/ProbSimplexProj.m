% Wang, Weiran, and Miguel A. Carreira-Perpin�n. "Projection onto the probability simplex: An efficient algorithm with a simple proof, and an application." arXiv preprint arXiv:1309.1541 (2013).
% The following vectorized Matlab code implements algorithm 1. It projects each row vector in the N � D
% matrix Y onto the probability simplex in D dimensions.

function X = ProbSimplexProj(Y)
[N,D] = size(Y);
X = sort(Y,2,'descend');
Xtmp = (cumsum(X,2)-1)*diag(sparse(1./(1:D)));
X = max(bsxfun(@minus,Y,Xtmp(sub2ind([N,D],(1:N)',sum(X>Xtmp,2)))),0);