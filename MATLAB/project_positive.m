function [s_projected] = project_positive(s)

s_projected = s;
s_projected(s<0) = 0;
end

