function val = sigmoid(x,a)
    val = 1./(1+exp(-a*x));
end