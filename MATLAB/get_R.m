function R = get_R(s,lambda,g,P)
%GET_R_TILDE Summary of this function goes here
%   Detailed explanation goes here

rho = get_rho(s,lambda,P);

s_cumsum = cumsum(s);
[s_mesh,G_mesh] = meshgrid(s_cumsum,g);
I = (G_mesh>=s_mesh);

R = I*rho;

end

