function grad_lambda_rho = get_grad_lambda_rho(s,lambda,P)
%GET_GRAD_S_RHO Summary of this function goes here
%   Detailed explanation goes here

% Verify column vec
s = s(:);
lambda = lambda(:);

% Constants
s_cumsum = cumsum(s); 
I_cumsum = 1-cumsum(lambda);
M = numel(s);

% Derivatives file, eq. (5)
% j>m
denum1 = 1+s_cumsum*P.*(lambda+I_cumsum);
denum2 = 1+s_cumsum*P.*I_cumsum;
tmp = -1/log(2)*s_cumsum.^2*P^2.*lambda./denum1./denum2; % Mx1 vector
% j=m
tmp2 = 1/log(2)*s_cumsum*P./denum1;


tmp3 = tril(repmat(tmp.',M,1),-1); % MxM matrix, column m is the gradient for s^m
grad_lambda_rho = tmp3+diag(tmp2);
end

