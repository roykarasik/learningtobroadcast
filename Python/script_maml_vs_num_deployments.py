# -*- coding: utf-8 -*-
"""
Created on Sun Nov  7 09:18:26 2021

@author: royka
"""

import torch
import numpy as np
import my_lib
import meta_lib
from multiprocessing import Pool
import time
from datetime import datetime
import pickle

def save_workspace(filename, vars_dict):
    with open(filename,'wb') as f:
        pickle.dump(vars_dict, f)
        
    return

# Get data and time for unique filename
now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
pickle_filename = './pickles/script_maml_vs_num_deployments_' + dt_string + '.pkl'

print(f'Starting time: {now}')
print('')

# Parameters
M = 6;
P = 100
num_N = 1
N_curr_vec = np.array([10])
N_prev_vec = np.array([10])
num_datasets = int(100)
beta = 0.1
flag_print_stats = 0
flag_print_graph = 0
num_prev_deploy = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30]
#num_prev_deploy = [10] # DEBUG
validation_percentage = 0.0
K = 1

# Constants
N_curr_vec = N_curr_vec.astype(int)
N_prev_vec = N_prev_vec.astype(int)
N_curr_validation_vec = np.rint(N_curr_vec*validation_percentage)
N_curr_validation_vec = N_curr_validation_vec.astype(int)
N_curr_training_vec = N_curr_vec-N_curr_validation_vec
N_prev_validation_vec = np.rint(N_prev_vec*validation_percentage)
N_prev_validation_vec = N_prev_validation_vec.astype(int)
N_prev_training_vec = N_prev_vec-N_prev_validation_vec
num_deploy_options = len(num_prev_deploy)


# Draw means and vars for deployments
curr_vars = 5*np.ones(K)
curr_means = np.sqrt(10) + np.zeros(K)
prev_vars = curr_vars*np.ones((K,num_prev_deploy[-1]))
means_factor = np.sqrt(2)
prev_means = curr_means + means_factor*np.random.normal(0, 1, (K,num_prev_deploy[-1]))
#prev_means = curr_means + np.zeros((K,num_prev_deploy)) # DEBUG

# MAML datasets 
G_meta_training_per_N, G_meta_validation_per_N = meta_lib.prepare_meta_datasets(N_prev_training_vec, N_prev_validation_vec, num_prev_deploy[-1], num_datasets, K, prev_vars, prev_means)
G_meta_training = G_meta_training_per_N[0]
G_meta_validation = G_meta_validation_per_N[0]

# Dataset for new deplyment
# Validation
G_list = my_lib.GMM_channel_extended_inputs(N_curr_validation_vec, num_datasets, K, curr_vars, curr_means)
G_validation = torch.from_numpy(np.transpose(G_list[0]))
# Training
G_list = my_lib.GMM_channel_extended_inputs(N_curr_training_vec, num_datasets, K, curr_vars, curr_means)
G_training = torch.from_numpy(np.transpose(G_list[0]))

# For validation_percentage = 0, set validation set to be training set (just or the algorithm to run as is)
if validation_percentage == 0:
    G_meta_validation = G_meta_training
    G_validation = G_training
    
# Channel coefficients for montecarlo 
g_mc = my_lib.get_ch_coeff_for_mc_extended_input(K, curr_vars, curr_means)

# Prepare dictionary of scenario
vars_dict = {}
vars_dict['M'] = M
vars_dict['P'] = P
vars_dict['N_curr_vec'] = N_curr_vec
vars_dict['N_prev_vec'] = N_prev_vec
vars_dict['num_datasets'] = num_datasets
vars_dict['beta'] = beta
vars_dict['num_prev_deploy'] = num_prev_deploy
vars_dict['validation_percentage'] = validation_percentage
vars_dict['N_curr_validation_vec'] = N_curr_validation_vec
vars_dict['N_curr_training_vec'] = N_curr_training_vec
vars_dict['N_prev_validation_vec'] = N_prev_validation_vec
vars_dict['N_prev_training_vec'] = N_prev_training_vec
vars_dict['curr_vars'] = curr_vars
vars_dict['curr_means'] = curr_means
vars_dict['prev_vars'] = prev_vars
vars_dict['prev_means'] = prev_means
vars_dict['means_factor'] = means_factor
vars_dict['G_meta_training'] = G_meta_training
vars_dict['G_meta_validation'] = G_meta_validation
vars_dict['G_training'] = G_training
vars_dict['G_validation'] = G_validation
#save_workspace(pickle_filename, vars_dict)

# Wrapper function to parallel
def wrapper_function(idx,D):
    
    # Choose D deplyments at random
    I = np.random.permutation(num_prev_deploy[-1])
    I = I[:D]
    #I = range(D) # for choosing first D deplyments always
    
    # Meta data for current scenario (Choose D deployments at random)
    g_meta_training = G_meta_training[idx][I,:]
    g_meta_validation = G_meta_validation[idx][I,:]
    
    # data for current scenario
    g_training  = G_training[idx:idx+1,:]
    g_validation = G_validation[idx:idx+1,:]
    
    # Random init for MAML
    u0, l0, s0, CVaR_surr_start = meta_lib.meta_random_init_with_local_search(g_meta_training,M,beta,P)
    
    # MAML
    meta_obj, u_maml, l_maml, s_maml = meta_lib.maml(u0,l0,g_meta_training,g_meta_validation,beta,P,flag_print_stats,flag_print_graph)
    
    # Testing CVaR with MAML initialization
    CVaR_maml = my_lib.get_empirical_CVaR(s_maml, l_maml, g_mc, beta, P)
    
    # Train new deployment based on MAML initialization
    CVaR_surr_empir, u_best, l_best, s_best = meta_lib.base_learner(u_maml,l_maml,g_training,g_validation,beta,P,flag_print_stats,flag_print_graph)
    
    # Testing CVaR with locally learned parameters
    CVaR_local = my_lib.get_empirical_CVaR(s_best, l_best, g_mc, beta, P)
    
    # Train new deployment with single step
    CVaR_surr_empir_single, u_single, l_single, s_single = meta_lib.base_learner_single_step(u_maml,l_maml,g_training,g_validation,beta,P,flag_print_stats,flag_print_graph)
    CVaR_local_single = my_lib.get_empirical_CVaR(s_single, l_single, g_mc, beta, P)
    
    # Comparing to random initialization with no MAML
    u1, l1, s1, CVaR_surr_start = my_lib.random_init_with_local_search(g_training,M,beta,P)
    u1 = torch.from_numpy(u1)
    l1 = torch.from_numpy(l1)
    u1.requires_grad = True
    l1.requires_grad = True
    tmp, u_no_maml, l_no_maml, s_no_maml = meta_lib.base_learner(u1,l1,g_training,g_validation,beta,P,flag_print_stats,flag_print_graph)
    CVaR_local_no_maml = my_lib.get_empirical_CVaR(s_no_maml, l_no_maml, g_mc, beta, P)
    
    # returns
    return [CVaR_maml, CVaR_local, CVaR_local_single, CVaR_local_no_maml]

# Prallel computation
def parallel_CVaR(D):
    my_pool = Pool() # (4)
    
    # Generate input for wrapper function
    wrapper_input = [(n, D) for n in range(num_datasets)]
    
    # Buffers to hold results
    CVaR_maml_array = np.empty(num_datasets)
    CVaR_local_array = np.empty(num_datasets)
    CVaR_local_single_array = np.empty(num_datasets)
    CVaR_local_no_maml_array = np.empty(num_datasets)
    
    # Pallel loop
    output_list = my_pool.starmap(wrapper_function, wrapper_input)
    
    # Rearange outputs
    for idx in range(num_datasets):
        CVaR_maml_array[idx] = output_list[idx][0]
        CVaR_local_array[idx] = output_list[idx][1]
        CVaR_local_single_array[idx] = output_list[idx][2]
        CVaR_local_no_maml_array[idx] = output_list[idx][3]
        
    return CVaR_maml_array, CVaR_local_array, CVaR_local_single_array, CVaR_local_no_maml_array

# Run over all N
if __name__ == '__main__':
    CVaR_maml_array = np.zeros((num_datasets,num_deploy_options))
    CVaR_local_array = np.zeros((num_datasets,num_deploy_options))
    CVaR_local_single_array = np.zeros((num_datasets,num_deploy_options))
    CVaR_local_no_maml_array = np.zeros((num_datasets,num_deploy_options))
    for idx_D, D in enumerate(num_prev_deploy):
        print(f'{idx_D+1}/{num_deploy_options}: D = {D}')
        t = time.time()
        CVaR_maml_array[:,idx_D], CVaR_local_array[:,idx_D], CVaR_local_single_array[:,idx_D], CVaR_local_no_maml_array[:,idx_D] = parallel_CVaR(D)
        elapsed = time.time() - t
        print(f'Total runtime = {elapsed:.2f} sec')
    
        # Average over all datasets (and exclude NaNs that can appear for extremely small datasets due to high and unlikely channel gains)
        CVaR_maml_mean = np.nanmean(CVaR_maml_array,0)
        CVaR_local_mean = np.nanmean(CVaR_local_array,0)
        CVaR_local_single_mean = np.nanmean(CVaR_local_single_array,0)
        CVaR_local_no_maml_mean = np.nanmean(CVaR_local_no_maml_array,0)
        print(f'CVaR_maml_mean = {CVaR_maml_mean[idx_D]:.4f}')
        print(f'CVaR_local_mean = {CVaR_local_mean[idx_D]:.4f}')
        print(f'CVaR_local_single_mean = {CVaR_local_single_mean[idx_D]:.4f}')
        print(f'CVaR_local_no_maml_mean = {CVaR_local_no_maml_mean[idx_D]:.4f}')
        
        # Add to variables dictionary
        vars_dict['CVaR_maml_array'] = CVaR_maml_array
        vars_dict['CVaR_local_array'] = CVaR_local_array
        vars_dict['CVaR_local_single_array'] = CVaR_local_single_array
        vars_dict['CVaR_local_no_maml_array'] = CVaR_local_no_maml_array
        vars_dict['CVaR_maml_mean'] = CVaR_maml_mean
        vars_dict['CVaR_local_mean'] = CVaR_local_mean
        vars_dict['CVaR_local_single_mean'] = CVaR_local_single_mean
        vars_dict['CVaR_local_no_maml_mean'] = CVaR_local_no_maml_mean
    
        # Save variables of interest
        print('Saving workspace')
        save_workspace(pickle_filename, vars_dict)
        print('')
            
    print('Done')
