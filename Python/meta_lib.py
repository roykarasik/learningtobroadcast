# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 09:45:23 2021

@author: royka
"""

import torch
import numpy as np
import my_lib
from torch.autograd import grad
import matplotlib.pyplot as plt

#device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = torch.device('cpu')

dtype = torch.float

##############################################################################
def prepare_meta_datasets_same_channel(N_training, N_validation, num_deploy, num_datasets):
    
    # First step: each element in the list is for deplyment
    G_meta_training_tmp = []
    G_meta_validation_tmp = []
    for idx in range(num_deploy):
        # Validation
        G_list, mixture_means, mixture_vars = my_lib.GMM_channel([N_validation], num_datasets)
        G = G_list[0]
        G_meta_validation_tmp.append(G)
        
        # Training
        G_list, mixture_means, mixture_vars = my_lib.GMM_channel([N_training], num_datasets)
        G = G_list[0]
        G_meta_training_tmp.append(G)
        
    # Second step: each element in the list is for a new draw
    G_meta_training = []
    G_meta_validation = []
    for idx in range(num_datasets):
        G_training = np.zeros((num_deploy, N_training))
        G_validation = np.zeros((num_deploy, N_validation))
        for idx_deply in range(num_deploy):
            G_training[idx_deply,:] = np.transpose(G_meta_training_tmp[idx_deply][:,idx])
            G_validation[idx_deply,:] = np.transpose(G_meta_validation_tmp[idx_deply][:,idx])
        G_meta_training.append(torch.from_numpy(G_training))
        G_meta_validation.append(torch.from_numpy(G_validation))
        
    return G_meta_training, G_meta_validation
##############################################################################

##############################################################################
def prepare_meta_datasets_same_channel_with_subsets(N_training_vec, N_validation_vec, num_deploy, num_datasets):
    
    # Constants
    num_N = N_training_vec.shape[0]
    
    # First step: each element in the list is for deplyment
    G_meta_training_tmp = []
    G_meta_validation_tmp = []
    for idx in range(num_deploy):
        # Validation
        G_list, mixture_means, mixture_vars = my_lib.GMM_channel(N_validation_vec, num_datasets)
        G_meta_validation_tmp.append(G_list)
        
        # Training
        G_list, mixture_means, mixture_vars = my_lib.GMM_channel(N_training_vec, num_datasets)
        G_meta_training_tmp.append(G_list)
        
    # Second step: each element in the list is for a new draw
    G_meta_training_per_N = []
    G_meta_validation_per_N = []
    for idx_N in range(num_N):
        G_meta_training = []
        G_meta_validation = []
        N_training = N_training_vec[idx_N]
        N_validation = N_validation_vec[idx_N]
        for idx in range(num_datasets):
            G_training = np.zeros((num_deploy, N_training))
            G_validation = np.zeros((num_deploy, N_validation))
            for idx_deploy in range(num_deploy):
                G_training[idx_deploy,:] = np.transpose(G_meta_training_tmp[idx_deploy][idx_N][:,idx])
                G_validation[idx_deploy,:] = np.transpose(G_meta_validation_tmp[idx_deploy][idx_N][:,idx])
            G_meta_training.append(torch.from_numpy(G_training))
            G_meta_validation.append(torch.from_numpy(G_validation))
        G_meta_training_per_N.append(G_meta_training)
        G_meta_validation_per_N.append(G_meta_validation)
        
    return G_meta_training_per_N, G_meta_validation_per_N
##############################################################################

##############################################################################
def prepare_meta_datasets(N_training_vec, N_validation_vec, num_deploy, num_datasets, K, mixture_vars, mixture_means):
    
    # Constants
    num_N = N_training_vec.shape[0]
    
    # First step: each element in the list is for deplyment
    G_meta_training_tmp = []
    G_meta_validation_tmp = []
    for idx in range(num_deploy):
        # Validation
        G_list = my_lib.GMM_channel_extended_inputs(N_validation_vec, num_datasets, K, mixture_vars[:,idx], mixture_means[:,idx])
        G_meta_validation_tmp.append(G_list)
        
        # Training
        G_list = my_lib.GMM_channel_extended_inputs(N_training_vec, num_datasets, K, mixture_vars[:,idx], mixture_means[:,idx])
        G_meta_training_tmp.append(G_list)
        
    # Second step: each element in the list is for a new draw
    G_meta_training_per_N = []
    G_meta_validation_per_N = []
    for idx_N in range(num_N):
        G_meta_training = []
        G_meta_validation = []
        N_training = N_training_vec[idx_N]
        N_validation = N_validation_vec[idx_N]
        for idx in range(num_datasets):
            G_training = np.zeros((num_deploy, N_training))
            G_validation = np.zeros((num_deploy, N_validation))
            for idx_deploy in range(num_deploy):
                G_training[idx_deploy,:] = np.transpose(G_meta_training_tmp[idx_deploy][idx_N][:,idx])
                G_validation[idx_deploy,:] = np.transpose(G_meta_validation_tmp[idx_deploy][idx_N][:,idx])
            G_meta_training.append(torch.from_numpy(G_training))
            G_meta_validation.append(torch.from_numpy(G_validation))
        G_meta_training_per_N.append(G_meta_training)
        G_meta_validation_per_N.append(G_meta_validation)
        
    return G_meta_training_per_N, G_meta_validation_per_N
##############################################################################

##############################################################################
def meta_random_init_with_local_search(g,M,beta,P):
    # Parameters
    num_draws = 100
    
    # Constants
    num_deploy = g.shape[0]
    
    CVaR_surr_emp_best = 0
    for idx in range(num_draws):
        # Random l0
        l0 = torch.rand(M)
        l0 /= torch.linalg.norm(l0, ord = 1)
        
        # Random u0
        exp_lambda = M
        s0 = torch.empty(M).exponential_(exp_lambda)
        s0, I = torch.sort(s0) # to avoid s_{0,1} to large (which zeros the gradient)
        u0 = torch.log(s0)
        
        # Calculate surrogate empirical CVaR. g is a matrix where each row corresponds to a deplyment. We are looking for initiazliation that maximizes the average over all deplyments
        tmp = [my_lib.get_surrogate_empirical_CVaR(s0, l0, g[idx_deploy:idx_deploy+1,:], beta, P).numpy() for idx_deploy in range(num_deploy)]
        CVaR_surr_emp = np.mean(tmp)
        
        # Check best
        if CVaR_surr_emp > CVaR_surr_emp_best:
            CVaR_surr_emp_best = CVaR_surr_emp
            s0_best = s0
            l0_best = l0
            u0_best = u0
    
    return u0_best.numpy(), l0_best.numpy(), s0_best.numpy(), CVaR_surr_emp_best
##############################################################################

##############################################################################
def inner_and_outer_updates(u,l,g_meta_training,g_meta_validation,beta,P):
    # Parameters
    eta = 0.01
    gamma = 0.01
    eta_meta = 0.01
    gamma_meta = 0.01
    
    # Constants
    num_deploy = g_meta_training.shape[0]
    
    # Rate-allocation vector
    s = torch.exp(u) 
      
    # Inner update
    u_tau = []
    s_tau = []
    l_tau = []
    obj = []
    grad_obj_u = []
    grad_obj_l = []
    meta_obj = torch.zeros(1)
    for idx_deploy in range(num_deploy):
        g_training = g_meta_training[idx_deploy:idx_deploy+1,:]
        g_validation = g_meta_validation[idx_deploy:idx_deploy+1,:]
        
        # Inner objective (based on training data)
        obj.append(my_lib.get_surrogate_empirical_CVaR(s, l, g_training, beta, P))
        
        # Inner gradients
        grad_obj_u.append(grad(obj[idx_deploy], u, create_graph=True)[0])
        grad_obj_l.append(grad(obj[idx_deploy], l, create_graph=True)[0])
        
        # Inner gradient descent
        u_tau.append(u + eta * grad_obj_u[idx_deploy])
        s_tau.append(torch.exp(u_tau[idx_deploy]))
        
        # Inner mirror descent
        l_tau.append(l*torch.exp(gamma*grad_obj_l[idx_deploy]))
        l_tau[idx_deploy] /= torch.sum(l_tau[idx_deploy])
        
        # Meta objective (based on validation data)
        meta_obj += my_lib.get_surrogate_empirical_CVaR(s_tau[idx_deploy], l_tau[idx_deploy], g_validation, beta, P)
    meta_obj /= num_deploy
    
    # Outer gradients
    grad_meta_obj_u = grad(meta_obj, u, create_graph=True)[0]
    grad_meta_obj_l = grad(meta_obj, l, create_graph=True)[0]
    
    # Parameters update
    with torch.no_grad():
        # Outer gradient descent
        u += eta_meta * grad_meta_obj_u
    
        # Outer mirror descent
        l *= torch.exp(gamma_meta*grad_meta_obj_l)
        l /= torch.sum(l)
        
        # Manually zero the gradients after updating weights
        u.grad = None
        l.grad = None
        
        # Evaluate objective with updated parameters
        s = torch.exp(u)
        meta_obj_after_descent = torch.zeros(1)
        for idx_deploy in range(num_deploy):
            g_validation = g_meta_validation[idx_deploy:idx_deploy+1,:]
            meta_obj_after_descent += my_lib.get_surrogate_empirical_CVaR(s, l, g_validation, beta, P)
        meta_obj_after_descent /= num_deploy
        meta_obj_after_descent = meta_obj_after_descent.detach().numpy()
    
    return meta_obj_after_descent, u, l
    
##############################################################################    

##############################################################################
def maml(u0,l0,g_meta_training,g_meta_validation,beta,P,flag_print_stats,flag_print_graph):
    # Parameters
    min_num_epochs = 500
    ratio_th = 1+5e-6
    
    # Constants
    M = l0.shape[0]
    num_epochs_display = int(0.01*min_num_epochs)
    
    # Init tensors with input u0 and l0
    u0 = torch.from_numpy(u0)
    l0 = torch.from_numpy(l0)
    if device.type == 'cuda':
        u0 = u0.to(device)
        l0 = l0.to(device)
    u = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    l = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    with torch.no_grad():
        u += u0
        l += l0
    
    # Parameters update
    meta_obj_best = 0
    idx = 0
    ratio = 2
    history = np.zeros(1)
    while (idx<min_num_epochs) or (ratio > ratio_th):
        meta_obj, u, l = inner_and_outer_updates(u,l,g_meta_training,g_meta_validation,beta,P)
        
        if idx>0: # first ratio is inf
            ratio = meta_obj/meta_obj_best
            
        # Save history if print_flag
        if flag_print_graph:
            history = np.append(history, meta_obj)
            
        # Check for best
        if meta_obj > meta_obj_best:
            meta_obj_best = meta_obj
            l_best = l
            u_best = u
            s_best = torch.exp(u)
        
        # Prints
        if (flag_print_stats) and ((idx+1) % num_epochs_display == 0):
            print(f'\rEpoch {idx+1}/{min_num_epochs}: meta-objective = {meta_obj[0]:.4f}', end = '')
            
        idx += 1
    
    print('')
        
    # Print epochs
    if flag_print_graph:
        plt.plot(history[1:])
        plt.show()
        
    return meta_obj_best, u_best.detach(), l_best.detach(), s_best.detach()
##############################################################################

##############################################################################
def inner_update(u,l,g_training,beta,P):
    # Parameters
    eta = 0.01
    gamma = 0.01
    
    # Rate-allocation vector
    s = torch.exp(u)
    
    # Objective
    obj = my_lib.get_surrogate_empirical_CVaR(s, l, g_training, beta, P)
    
    # Gradients
    grad_obj_u = grad(obj, u, create_graph=True)[0]
    grad_obj_l = grad(obj, l, create_graph=True)[0]
    
    # Parameters update
    with torch.no_grad():
        # Gradient descent
        u += eta * grad_obj_u
    
        # Mirror descent
        l *= torch.exp(gamma*grad_obj_l)
        l /= torch.sum(l)
        
        """
        # Check for NaNs
        if torch.isnan(l).any() == True:
            l = torch.ones(M)/M # ?
        """
        
        # Manually zero the gradients after updating weights
        u.grad = None
        l.grad = None
    
    return u, l
##############################################################################

##############################################################################
def base_learner(u0,l0,g_training,g_validation,beta,P,flag_print_stats,flag_print_graph):
    # Parameters
    min_num_epochs = 500
    ratio_th = 1+5e-6
    number_of_iterations_between_tests = 1
    
    # Constants
    num_epochs_display = int(0.01*min_num_epochs)
    M = l0.shape[0]
    
    # Init
    u = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    l = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    with torch.no_grad():
        u += u0
        l += l0
    l_best = l
    u_best = u
    s = torch.exp(u) 
    s_best = s
    
    # Parameters update
    obj_best = 0
    idx = 0
    ratio = 2
    history_training = np.zeros(1)
    history_validation = np.zeros(1)
    while (idx<min_num_epochs) or (ratio > ratio_th):
        # Descent
        u, l = inner_update(u,l,g_training,beta,P)
        
        # Rate-allocation vector
        s = torch.exp(u)    
        
        # Evaluate training
        obj_training = my_lib.get_surrogate_empirical_CVaR(s, l, g_training, beta, P).detach().numpy()
        
        # Save history if print_flag
        if flag_print_graph:
            history_training = np.append(history_training, obj_training)
                    
        if idx % number_of_iterations_between_tests == 0:
        
            # Evaluate validation 
            obj_validation = my_lib.get_surrogate_empirical_CVaR(s, l, g_validation, beta, P).detach().numpy()
        
            # Save history if print_flag
            if flag_print_graph:
                history_validation = np.append(history_validation, obj_validation)
        
            # Check for best
            if obj_validation > obj_best:
                obj_best = obj_validation
                l_best = l
                u_best = u
                s_best = s
            
            # Prints
            if (flag_print_stats) and ((idx+1) % num_epochs_display == 0):
                print(f'\rEpoch {idx+1}/{min_num_epochs}: Objective (training) = {obj_training[0]:.4f}, Objective (validation) = {obj_validation[0]:.4f}', end = '')
        
            if idx>0: # first ratio is inf
                ratio = obj_validation/obj_best
                #ratio = obj_training/obj_best
            
        idx += 1
    
    print('')
    
    # Print epochs
    if flag_print_graph:
        plt.plot(history_training[1:], label="Training")
        plt.plot(history_validation[1:], label="Validation")
        plt.legend()
        plt.show()
        
    return obj_best, u_best.detach(), l_best.detach(), s_best.detach()
##############################################################################

##############################################################################
def base_learner_single_step(u0,l0,g_training,g_validation,beta,P,flag_print_stats,flag_print_graph):
    # Constants
    M = l0.shape[0]
    
    # Init
    u = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    l = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    with torch.no_grad():
        u += u0
        l += l0
    
    # Descent
    u, l = inner_update(u,l,g_training,beta,P)
    
    # Rate-allocation vector
    s = torch.exp(u)    
   
    # Evaluate validation 
    obj_validation = my_lib.get_surrogate_empirical_CVaR(s, l, g_validation, beta, P).detach().numpy()
    
    
    return obj_validation, u.detach(), l.detach(), s.detach()
##############################################################################