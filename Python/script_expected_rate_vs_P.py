# -*- coding: utf-8 -*-
"""
Created on Mon Nov 15 09:20:57 2021

@author: royka
"""

import torch
import numpy as np
import my_lib
from multiprocessing import Pool
import time
from datetime import datetime
import pickle

def save_workspace(filename, vars_dict):
    with open(filename,'wb') as f:
        pickle.dump(vars_dict, f)
        
    return

# Get data and time for unique filename
now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
pickle_filename = './pickles/script_cvar_vs_P_' + dt_string + '.pkl'

print(f'Starting time: {now}')
print('')

# Parameters
M_vec = [1,2,6];
P_vec = np.logspace(0, 4, num=9)
N = int(1e3)
num_datasets = int(1e3)
flag_print_stats = 0
flag_print_graph = 0
K = 1
var = 1
mu = np.sqrt(0)
num_mc = int(1e7)

# Dataset for each var
print('Preparing datasets')
# Local dataset
ch_var = var*np.ones(K)
ch_means = mu+np.zeros(K)
G_list = my_lib.GMM_channel_extended_inputs([N], num_datasets, K, ch_var, ch_means)
G = torch.from_numpy(np.transpose(G_list[0]))
# Channel coefficients for montecarlo 
G_list = my_lib.GMM_channel_extended_inputs([int(num_mc/num_datasets)], num_datasets, K, ch_var, ch_means)
G_tmp = G_list[0]
G_tmp = np.reshape(G_tmp,(num_mc,1))
g_mc = torch.from_numpy(np.transpose(G_tmp))
print('Done')
print('')

# Prepare dictionary of scenario
vars_dict = {}
vars_dict['M_vec'] = M_vec
vars_dict['P_vec'] = P_vec
vars_dict['N'] = N
vars_dict['num_datasets'] = num_datasets
vars_dict['K'] = K
vars_dict['var'] = var
vars_dict['mu'] = mu
vars_dict['num_mc'] = num_mc
#vars_dict['ch_G'] = ch_G
#vars_dict['ch_g_mc'] = ch_g_mc

"""
# DEBUG
idx_dataset = 0
P = 100
M = 1
g = G[idx_dataset:idx_dataset+1,:]
u0, l0, s0, rate_start = my_lib.random_init_with_local_search(g,M,1,P)
u0 = torch.from_numpy(u0)
l0 = torch.from_numpy(l0)
u0.requires_grad = True
l0.requires_grad = True
tmp, u, l, s = my_lib.base_learner(u0,l0,g,1,P,1,1)
expected = my_lib.get_empirical_CVaR(s, l, g_mc, 1, P)
"""

# Wrapper function to parallel
def wrapper_function(idx_dataset, P, M):
    # Dataset for current scenario
    g = G[idx_dataset:idx_dataset+1,:]

    
    # Random starting point
    u0, l0, s0, rate_start = my_lib.random_init_with_local_search(g,M,1,P)
    u0 = torch.from_numpy(u0)
    l0 = torch.from_numpy(l0)
    u0.requires_grad = True
    l0.requires_grad = True
        
    # Optimize expected rate
    tmp, u, l, s = my_lib.base_learner(u0,l0,g,1,P,flag_print_stats,flag_print_graph)
    expected = my_lib.get_empirical_CVaR(s, l, g_mc, 1, P)
      
    # returns
    return expected

# Prallel computation
def parallel_fun(P, M):
    my_pool = Pool() # (4)
    
    # Generate input for wrapper function
    wrapper_input = [(n, P, M) for n in range(num_datasets)]
    
    # Pallel loop
    output_list = my_pool.starmap(wrapper_function, wrapper_input)
    
    # Rearange outputs
    rate = np.empty(num_datasets)
    for idx in range(num_datasets):
        rate[idx] = output_list[idx]
           
    return rate

if __name__ == '__main__':
    rate_array = np.zeros((num_datasets,len(P_vec),len(M_vec)))
    for idx_P, P in enumerate(P_vec):
        for idx_M, M in enumerate(M_vec):
            print(f'P={P}, M={M}')
            t = time.time()
            rate_array[:,idx_P,idx_M] = parallel_fun(P, M)
            elapsed = time.time() - t
            print(f'Total runtime = {elapsed:.2f} sec')
        
            # Average over all datasets 
            rate_mean = np.nanmean(rate_array,0)
            print(f'rate_mean = {rate_mean[idx_P,idx_M]:.4f}')
            
            # Add to variables dictionary
            vars_dict['rate_array'] = rate_array
            vars_dict['rate_mean'] = rate_mean
        
            # Save variables of interest
            print('Saving workspace')
            save_workspace(pickle_filename, vars_dict)
            print('')
            
    print('Done')