# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 09:51:45 2021

@author: royka
"""

import torch
import numpy as np
from torch.utils.data import Dataset
from torch.autograd import grad
import matplotlib.pyplot as plt

#device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = torch.device('cpu')

dtype = torch.float

##############################################################################
def GMM_channel(N_vec, num_datasets):
    # Number of Gaussians
    K = 1
    
    # Init channel coefficients for largest dataset
    N_max = np.max(N_vec)
    h = np.zeros((N_max, num_datasets), dtype=complex)
    
    # Mixture probabilities
    prob_mixture = 1/K*np.ones(K)
    cdf_mixture = np.append(0, np.cumsum(prob_mixture))
    
    # Mixture variances
    #mixture_vars = 16*np.ones(K)
    mixture_vars = 1*np.ones(K)
    
    # Mixture means
    #mixture_means = np.sqrt(20) + np.zeros(K)
    mixture_means = np.sqrt(0) + np.zeros(K)
    
    # Draw kernel index (ala Inverse transform sampling)
    u = np.random.random(N_max)
    kernel_inds = np.zeros(N_max);
    for idx in range(N_max):
        n_I = np.random.normal(0, 1, num_datasets)
        n_Q = np.random.normal(0, 1, num_datasets)
        idx_cdf_greater = np.where(cdf_mixture >= u[idx])[0][0]-1 # minus 1 is due to the artifical "0" in cdf_mixture
        kernel_inds[idx] = idx_cdf_greater
        h[idx,:] = mixture_means[idx_cdf_greater]+np.sqrt(mixture_vars[idx_cdf_greater]/2)*(n_I+1j*n_Q)
    
    # Take subsets for each N
    G_list = []
    for idx, N in enumerate(N_vec):
        h_subset = h[:N,:]
        G_subset = np.abs(h_subset) ** 2
        G_subset_sorted = np.sort(G_subset,0)
        G_list.append(G_subset_sorted)
    
    return G_list, mixture_means, mixture_vars
##############################################################################

##############################################################################
def GMM_channel_extended_inputs(N_vec, num_datasets, K, mixture_vars, mixture_means):
    
    # Init channel coefficients for largest dataset
    N_max = np.max(N_vec)
    h = np.zeros((N_max, num_datasets), dtype=complex)
    
    # Mixture probabilities
    prob_mixture = 1/K*np.ones(K)
    cdf_mixture = np.append(0, np.cumsum(prob_mixture))
    
   
    # Draw kernel index (ala Inverse transform sampling)
    u = np.random.random(N_max)
    kernel_inds = np.zeros(N_max);
    for idx in range(N_max):
        n_I = np.random.normal(0, 1, num_datasets)
        n_Q = np.random.normal(0, 1, num_datasets)
        idx_cdf_greater = np.where(cdf_mixture >= u[idx])[0][0]-1 # minus 1 is due to the artifical "0" in cdf_mixture
        kernel_inds[idx] = idx_cdf_greater
        h[idx,:] = mixture_means[idx_cdf_greater]+np.sqrt(mixture_vars[idx_cdf_greater]/2)*(n_I+1j*n_Q)
    
    # Take subsets for each N
    G_list = []
    for idx, N in enumerate(N_vec):
        h_subset = h[:N,:]
        G_subset = np.abs(h_subset) ** 2
        G_subset_sorted = np.sort(G_subset,0)
        G_list.append(G_subset_sorted)
    
    return G_list
##############################################################################

##############################################################################
def get_ch_coeff_for_mc():
    # Parameters
    num_mc = int(1e7)
      
    G_mc_list, tmp1, tmp2 =  GMM_channel([1], num_mc)
    g_mc = G_mc_list[0]
    #g_mc = g_mc[0,:]
    g_mc = np.sort(g_mc,1)
    g_mc = torch.from_numpy(g_mc)
    if device.type == 'cuda':
        g_mc = g_mc.to(device)
    
    return g_mc
##############################################################################

##############################################################################
def get_ch_coeff_for_mc_extended_input(K, mixture_vars, mixture_means):
    # Parameters
    num_mc = int(1e7)
      
    G_mc_list =  GMM_channel_extended_inputs([1], num_mc, K, mixture_vars, mixture_means)
    g_mc = G_mc_list[0]
    #g_mc = g_mc[0,:]
    g_mc = np.sort(g_mc,1)
    g_mc = torch.from_numpy(g_mc)
    if device.type == 'cuda':
        g_mc = g_mc.to(device)
    
    return g_mc
##############################################################################

##############################################################################
def get_rho(s,l,P):
    s_cumsum = torch.cumsum(s,0)
    I_cumsum = 1-torch.cumsum(l,0)
    numerator = s_cumsum*l*P
    denominator = 1+s_cumsum*I_cumsum*P
    val = torch.log2(1+numerator/denominator) # eq. (5)
    return val
##############################################################################

##############################################################################
def get_R_sigma(s,l,g,P):
    # Parameter
    c = 50 # 50 ,10 
    
    # Constants
    M = torch.numel(s)
    N = g.shape[1] #list(g.size())[1] #torch.numel(g)
    num_datasets = g.shape[0] #list(g.size())[0]
    
    rho = get_rho(s,l,P)
    s_cumsum = torch.cumsum(s,0)
    
    R_sigma = torch.tensor(np.zeros((num_datasets,N)))
    if device.type == 'cuda':
        R_sigma = R_sigma.to(device)
    for idx in range(M):
        R_sigma += rho[idx]*torch.sigmoid(c*(g-s_cumsum[idx]))
        
    return R_sigma
##############################################################################

##############################################################################
def get_R(s,l,g,P):
    # Constants
    M = torch.numel(s)
    N = g.shape[1] #list(g.size())[1] #torch.numel(g)
    num_datasets = g.shape[0] #list(g.size())[0]
    
    rho = get_rho(s,l,P)
    s_cumsum = torch.cumsum(s,0)
    
    R = torch.tensor(np.zeros((num_datasets,N)))
    if device.type == 'cuda':
        R = R.to(device)
    for idx in range(M):
        R += rho[idx]*(g>=s_cumsum[idx])
        
    return R
##############################################################################

##############################################################################
def get_surrogate_average_rate(s,l,g,P):
    R_sigma = get_R_sigma(s,l,g,P)
    return torch.mean(R_sigma,1)
##############################################################################

##############################################################################
def get_average_rate(s,l,g,P):
    R = get_R(s,l,g,P)
    return torch.mean(R,1)
##############################################################################

##############################################################################
def nearest_positive_integer(x):
    x = round(x)
    if x==0:
        x = 1
    return int(x)
##############################################################################

##############################################################################
def get_g_beta(g,beta):
    # constants
    N = g.shape[1] #list(g.size())[1] #torch.numel(g)
    N_beta_int = nearest_positive_integer(N*beta)
    
    return g[:,:N_beta_int] #g[:N_beta_int]
##############################################################################

##############################################################################
def get_surrogate_empirical_CVaR(s,l,g,beta,P):
    # constants
    N = g.shape[1] #list(g.size())[1] #torch.numel(g)
    N_beta_int = nearest_positive_integer(N*beta)
    N_beta = N*beta
    alpha = N_beta_int/N_beta
    
    # Subset g_beta
    g_beta = get_g_beta(g,beta)
    
    # eq. (36)
    arg1 = alpha*get_surrogate_average_rate(s,l,g_beta,P)
    all_R_sigma = get_R_sigma(s,l,g_beta,P)
    arg2 = (1-alpha)*all_R_sigma[:,-1]
    return arg1 + arg2 
##############################################################################

##############################################################################
def get_empirical_CVaR(s,l,g,beta,P):
    # constants
    N = g.shape[1] #list(g.size())[1] #torch.numel(g)
    N_beta_int = nearest_positive_integer(N*beta)
    N_beta = N*beta
    alpha = N_beta_int/N_beta
    
    # Subset g_beta
    g_beta = get_g_beta(g,beta)
    
    # eq. (29)
    arg1 = alpha*get_average_rate(s,l,g_beta,P)
    all_R = get_R(s,l,g_beta,P)
    arg2 = (1-alpha)*all_R[:,-1]
    return arg1 + arg2 
##############################################################################

##############################################################################
class fading_dataset(Dataset):
    def __init__(self, G):
        # Features
        self.G = G

    def __len__(self):
        return self.G.shape[1]

    def __getitem__(self, index):
        # Read from memory
        g_numpy = self.G[:,index]

        # To torch
        g = torch.from_numpy(g_numpy)
        
        # Move to GPU
        if device.type == 'cuda':
            g = g.to(device)

        # Remove dim 1
        #g = torch.squeeze(g)

        return g
##############################################################################

##############################################################################
def get_good_starting_point(dataset,M,beta,P):
    # Parameters
    num_starting_points = int(1e3)
    
    # constants
    num_datasets = dataset.__len__()
      
    # Dataloader
    dataloader = torch.utils.data.DataLoader(dataset, shuffle=False, batch_size=num_datasets)
    
    CVaR_best = 0
    for idx_point in range(num_starting_points):
        # Random
        l0 = torch.rand(M)
        l0 /= torch.linalg.norm(l0, ord = 1)
        s0 = np.random.exponential(1, M)
        s0 = torch.from_numpy(s0)
        if device.type == 'cuda':
            l0 = l0.to(device)
            s0 = s0.to(device)
                
        # Calculate average CVaR
        for idx, g in enumerate(dataloader):
            CVaR = get_empirical_CVaR(s0,l0,g,beta,P)
        
        CVaR_mean = torch.mean(CVaR)
        if CVaR_mean > CVaR_best:
            CVaR_best = CVaR_mean
            s0_best = s0
            l0_best = l0
    
    return np.log(s0_best.cpu().numpy()), l0_best.cpu().numpy(), CVaR_best.cpu().numpy()
##############################################################################

##############################################################################
def get_good_starting_point_mc(g_mc,M,beta,P):
    # Parameters
    num_starting_points = 300
    
    # constants
         
    CVaR_best = 0
    for idx_point in range(num_starting_points):
        # Random
        l0 = torch.rand(M)
        l0 /= torch.linalg.norm(l0, ord = 1)
        s0 = np.random.exponential(1, M)
        s0 = torch.from_numpy(s0)
        if device.type == 'cuda':
            l0 = l0.to(device)
            s0 = s0.to(device)
            
                
        # Calculate CVaR
        CVaR = get_empirical_CVaR(s0,l0,g_mc,beta,P)
        
        CVaR_mean = torch.mean(CVaR)
        if CVaR_mean > CVaR_best:
            CVaR_best = CVaR_mean
            s0_best = s0
            l0_best = l0
    
    return np.log(s0_best.cpu().numpy()), l0_best.cpu().numpy(), CVaR_best.cpu().numpy()
##############################################################################

##############################################################################
def random_init(M):
    l0 = np.random.random(M)
    l0 /= np.linalg.norm(l0, ord = 1)
    
    beta = 1/M
    s0 = np.random.exponential(beta, M)
    s0 = np.sort(s0) # to avoid s_{0,1} to large (which zeros the gradient)
    u0 = np.log(s0)
    
    return u0, l0, s0
##############################################################################

##############################################################################
def random_init_with_local_search(g,M,beta,P):
    # Parameters
    num_draws = 100
    
    CVaR_surr_emp_best = 0
    for idx in range(num_draws):
        # Random l0
        l0 = torch.rand(M)
        l0 /= torch.linalg.norm(l0, ord = 1)
        
        # Random u0
        exp_lambda = M
        s0 = torch.empty(M).exponential_(exp_lambda)
        s0, I = torch.sort(s0) # to avoid s_{0,1} to large (which zeros the gradient)
        u0 = torch.log(s0)
        
        # Calculate surrogate empirical CVaR
        CVaR_surr_emp = get_surrogate_empirical_CVaR(s0, l0, g, beta, P)
        
        # Check best
        if CVaR_surr_emp > CVaR_surr_emp_best:
            CVaR_surr_emp_best = CVaR_surr_emp
            s0_best = s0
            l0_best = l0
            u0_best = u0
    
    return u0_best.numpy(), l0_best.numpy(), s0_best.numpy(), CVaR_surr_emp_best.numpy()
##############################################################################

##############################################################################
def parameters_update(u,l,g,beta,P):
    # Parameters
    eta = 0.01
    gamma = 0.01
    
    # Rate-allocation vector
    s = torch.exp(u) 
    
    # Forward pass
    CVaR_surr_emp = get_surrogate_empirical_CVaR(s, l, g, beta, P)
    
    # Backward pass
    CVaR_surr_emp.backward()
    
    # Parameters update
    with torch.no_grad():
        # Gradient descent
        u += eta * u.grad
        
        # Mirror descent
        l = l*torch.exp(gamma*l.grad) # disables requires_grad for some reason...
        l_norm = torch.linalg.norm(l, ord = 1)
        l /= l_norm
        l.requires_grad = True

        # Manually zero the gradients after updating weights
        u.grad = None
        l.grad = None
        
        # Evaluate objective with updated parameters
        s = torch.exp(u) # based on updated u
        CVaR_surr_emp = get_surrogate_empirical_CVaR(s, l, g, beta, P)
        
        # Detach and transfer to numpy
        if device.type == 'cuda':
            CVaR_surr_emp = CVaR_surr_emp.cpu()
        CVaR_surr_emp = CVaR_surr_emp.detach().numpy()
    
    return u, l, CVaR_surr_emp
##############################################################################

##############################################################################
def train_surrogate_empirical_CVaR(u0,l0,g,beta,P,flag_print_stats,flag_print_graph):
    # Parameters
    min_num_epochs = 1000
    ratio_th = 1+5e-6
    
    # Constants
    M = l0.shape[0]
    dtype = torch.float
    num_epochs_display = int(0.01*min_num_epochs)
    
    # Init tensors with input u0 and l0
    u0 = torch.from_numpy(u0)
    l0 = torch.from_numpy(l0)
    if device.type == 'cuda':
        u0 = u0.to(device)
        l0 = l0.to(device)
    u = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    l = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    with torch.no_grad():
        u += u0
        l += l0
    
    
    # Parameters update
    CVaR_rate_best = 0
    idx = 0
    ratio = 2
    history = np.zeros(1)
    while (idx<min_num_epochs) or (ratio > ratio_th):
    #for idx in range(num_epochs):
        # Update
        u, l, CVaR_rate = parameters_update(u,l,g,beta,P)
        if idx>0: # first ratio is inf
            ratio = CVaR_rate/CVaR_rate_best
        # Save history if print_flag
        if flag_print_graph:
            history = np.append(history, CVaR_rate)
        # Check for best
        if CVaR_rate > CVaR_rate_best:
            CVaR_rate_best = CVaR_rate
            l_best = l
            u_best = u
            s_best = torch.exp(u_best)
        # Prints
        if (flag_print_stats) and ((idx+1) % num_epochs_display == 0):
            print(f'\rEpoch {idx+1}/{min_num_epochs}: Surrogate CVaR = {CVaR_rate[0]:.4f}', end = '')
        idx += 1
        
    print('')
        
    # Print epochs
    if flag_print_graph:
        plt.plot(history[1:])
        plt.show()
        
    return CVaR_rate_best, u_best.detach(), l_best.detach(), s_best.detach()
##############################################################################

##############################################################################
def inner_update(u,l,g,beta,P):
    # Parameters
    eta = 0.01
    gamma = 0.01
    
    # Rate-allocation vector
    s = torch.exp(u)
    
    # Objective
    obj = get_surrogate_empirical_CVaR(s, l, g, beta, P)
    
    # Gradients
    grad_obj_u = grad(obj, u, create_graph=True)[0]
    grad_obj_l = grad(obj, l, create_graph=True)[0]
    
    # Parameters update
    with torch.no_grad():
        # Gradient descent
        u += eta * grad_obj_u
    
        # Mirror descent
        l *= torch.exp(gamma*grad_obj_l)
        l /= torch.sum(l)
        
        # Manually zero the gradients after updating weights
        u.grad = None
        l.grad = None
    
    return u, l
##############################################################################

##############################################################################
def base_learner(u0,l0,g,beta,P,flag_print_stats,flag_print_graph):
    # Parameters
    min_num_epochs = 500 #1000
    ratio_th = 1+5e-6
    
    # Constants
    num_epochs_display = int(0.01*min_num_epochs)
    M = l0.shape[0]
    
    # Init
    u = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    l = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    with torch.no_grad():
        u += u0
        l += l0
    l_best = l
    u_best = u
    s = torch.exp(u) 
    s_best = s
    
    # Parameters update
    obj_best = 0
    idx = 0
    ratio = 2
    history = np.zeros(1)
    while (idx<min_num_epochs) or (ratio > ratio_th):
        # Descent
        u, l = inner_update(u,l,g,beta,P)
        
        # Rate-allocation vector
        s = torch.exp(u)    
        
        # Evaluate training
        obj = get_surrogate_empirical_CVaR(s, l, g, beta, P).detach().numpy()
        
        # Save history if print_flag
        if flag_print_graph:
            history = np.append(history, obj)
            
        if idx>0: # first ratio is inf
            ratio = obj/obj_best
                      
        # Check for best
        if obj > obj_best:
            obj_best = obj
            l_best = l
            u_best = u
            s_best = s
        
        # Prints
        if (flag_print_stats) and ((idx+1) % num_epochs_display == 0):
            print(f'\rEpoch {idx+1}/{min_num_epochs}: Objective = {obj[0]:.4f}', end = '')
            
        idx += 1
    
    print('')
    
    # Print epochs
    if flag_print_graph:
        plt.plot(history[1:], label="Training")
        plt.legend()
        plt.show()
        
    return obj_best, u_best.detach(), l_best.detach(), s_best.detach()
##############################################################################

##############################################################################
def inner_update_obj_outage(u,l,g,beta,P):
    # Parameters
    eta = 0.01
    gamma = 0.01
    
    # Rate-allocation vector
    s = torch.exp(u)
    
    # Objective
    g_beta = get_g_beta(g,beta)
    all_R_sigma = get_R_sigma(s,l,g_beta,P)
    obj = all_R_sigma[:,-1]
    
    # Gradients
    grad_obj_u = grad(obj, u, create_graph=True)[0]
    grad_obj_l = grad(obj, l, create_graph=True)[0]
    
    # Parameters update
    with torch.no_grad():
        # Gradient descent
        u += eta * grad_obj_u
    
        # Mirror descent
        l *= torch.exp(gamma*grad_obj_l)
        l /= torch.sum(l)
        
        # Manually zero the gradients after updating weights
        u.grad = None
        l.grad = None
    
    return u, l
##############################################################################

##############################################################################
def base_learner_obj_outage(u0,l0,g,beta,P,flag_print_stats,flag_print_graph):
    # Parameters
    min_num_epochs = 1000
    ratio_th = 1+5e-6
    
    # Constants
    num_epochs_display = int(0.01*min_num_epochs)
    M = l0.shape[0]
    g_beta = get_g_beta(g,beta)
    
    # Init
    u = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    l = torch.zeros(M, device=device, dtype=dtype, requires_grad=True)
    with torch.no_grad():
        u += u0
        l += l0
    l_best = l
    u_best = u
    s = torch.exp(u) 
    s_best = s
    
    # Parameters update
    obj_best = 0
    idx = 0
    ratio = 2
    history = np.zeros(1)
    while (idx<min_num_epochs) or (ratio > ratio_th):
        # Descent
        u, l = inner_update_obj_outage(u,l,g,beta,P)
        
        # Rate-allocation vector
        s = torch.exp(u)    
        
        # Evaluate training
        all_R_sigma = get_R_sigma(s,l,g_beta,P)
        obj = all_R_sigma[:,-1].detach().numpy()
        
        # Save history if print_flag
        if flag_print_graph:
            history = np.append(history, obj)
            
        if idx>0: # first ratio is inf
            ratio = obj/obj_best
                      
        # Check for best
        if obj > obj_best:
            obj_best = obj
            l_best = l
            u_best = u
            s_best = s
        
        # Prints
        if (flag_print_stats) and ((idx+1) % num_epochs_display == 0):
            print(f'\rEpoch {idx+1}/{min_num_epochs}: Objective = {obj[0]:.4f}', end = '')
                
        idx += 1
    
    print('')
    
    # Print epochs
    if flag_print_graph:
        plt.plot(history[1:], label="Training")
        plt.legend()
        plt.show()
        
    return obj_best, u_best.detach(), l_best.detach(), s_best.detach()
##############################################################################
