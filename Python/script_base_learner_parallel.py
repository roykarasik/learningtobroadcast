# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 10:13:58 2021

@author: royka
"""

import torch
import numpy as np
import my_lib
from multiprocessing import Pool
import time
from datetime import datetime
import pickle

def save_workspace(filename, M, P, N_vec, num_datasets, beta, G, g_mc, u0, l0, CVaR_array, CVaR_surrogate_array, mixture_means, mixture_vars):
    vars_dict = {'M': M, 'P': P, 'N_vec': N_vec, 'num_datasets': num_datasets, 'beta': beta, 'G': G, 'g_mc': g_mc, 'u0': u0, 'l0': l0, 'CVaR_surrogate': CVaR_surrogate_array, 'CVaR': CVaR_array, 'mixture_means': mixture_means, 'mixture_vars': mixture_vars }
    with open(filename,'wb') as f:
        pickle.dump(vars_dict, f)
        
    return

# Get data and time for unique filename
now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
pickle_filename = './pickles/script_base_learner_parallel_' + dt_string + '.pkl'

print(f'Starting time: {now}')

# Parameters
M = 6;
P = 100
N_vec = np.array([200])
num_datasets = int(1024)
beta = 0.01
batch_size = 1
flag_print_stats = 0
flag_print_graph = 0

# Datasets
G_list, mixture_means, mixture_vars = my_lib.GMM_channel(N_vec, num_datasets)
G = G_list[0]
dataset = my_lib.fading_dataset(G)
g_mc = my_lib.get_ch_coeff_for_mc()


# Dataloader
dataloader = torch.utils.data.DataLoader(dataset, shuffle=False, batch_size=batch_size)

"""
# Init
print('Drawing init point')
u0, l0, CVaR_Val = my_lib.get_good_starting_point(dataset,M,beta,P)
##u0, l0, CVaR_Val = my_lib.get_good_starting_point_mc(g_mc,M,beta,P)
print('Done')
print('')
"""
u0 = []
l0 = []

# Wrapper function to parallel
def wrapper_function(g):
    # Random init
    #u0, l0, s0 = my_lib.random_init(M)
    u0, l0, s0, CVaR_surr_start = my_lib.random_init_with_local_search(g,M,beta,P)
    
    # Surrogate Empirical CVaR Maximization
    CVaR_surrogate, u_best, l_best, s_best = my_lib.train_surrogate_empirical_CVaR(u0,l0,g,beta,P,flag_print_stats,flag_print_graph)
    
    # Estimating true CVaR
    CVaR_mc = my_lib.get_empirical_CVaR(s_best, l_best, g_mc, beta, P)
    
    #return CVaR_mc
    return CVaR_surrogate, CVaR_mc

# Compute CVaR for each dataset
def parallel_CVaR():
    my_pool = Pool(4)
    #CVaR_array = my_pool.map(wrapper_function, dataloader)
    #return torch.cat(CVaR_array).numpy()
    CVaR_array = np.empty(0)
    CVaR_surrogate_array = np.empty(0)
    for CVaR_surrogate, CVaR_mc in my_pool.map(wrapper_function, dataloader):
        CVaR_array = np.append(CVaR_array, CVaR_mc.numpy())
        CVaR_surrogate_array = np.append(CVaR_surrogate_array, CVaR_surrogate)
    return CVaR_array, CVaR_surrogate_array


if __name__ == '__main__':
    print('Starting parallel job')
    t = time.time()
    CVaR_array, CVaR_surrogate_array = parallel_CVaR()
    elapsed = time.time() - t
    print(f'Total runtime = {elapsed:.2f} sec')
    
    # Save variables of interest
    print('Saving workspace')
    save_workspace(pickle_filename, M, P, N_vec, num_datasets, beta, G, g_mc, u0, l0, CVaR_array, CVaR_surrogate_array, mixture_means, mixture_vars)
    
    # Average over all datasets
    CVaR_mean = np.mean(CVaR_array)
    CVaR_median = np.median(CVaR_array)
    print(f'Mean CVaR = {CVaR_mean:.4f}')
    print(f'Median CVaR = {CVaR_median:.4f}')
    
    print('Done')