# -*- coding: utf-8 -*-
"""
Created on Tue Nov  9 13:23:00 2021

@author: royka
"""

import torch
import numpy as np
import my_lib
from multiprocessing import Pool
import time
from datetime import datetime
import pickle

def save_workspace(filename, vars_dict):
    with open(filename,'wb') as f:
        pickle.dump(vars_dict, f)
        
    return

# Get data and time for unique filename
now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
pickle_filename = './pickles/script_cvar_vs_beta_' + dt_string + '.pkl'

print(f'Starting time: {now}')
print('')

# Parameters
M = 6;
P = 100
N = int(1e4)
num_datasets = int(1000)
beta_vec = [0.0005, 0.001, 0.005, 0.01, 0.03, 0.05, 0.1, 0.3, 0.5, 0.9, 1]
flag_print_stats = 0
flag_print_graph = 0
K = 1
curr_vars = 1*np.ones(K)
curr_means = np.sqrt(4) + np.zeros(K)

# constants 
num_betas = len(beta_vec)

G_list = my_lib.GMM_channel_extended_inputs([N], num_datasets, K, curr_vars, curr_means)
G_tmp = G_list[0]
G = torch.from_numpy(np.transpose(G_tmp))

# Channel coefficients for montecarlo 
g_mc = my_lib.get_ch_coeff_for_mc_extended_input(K, curr_vars, curr_means)

# Prepare dictionary of scenario
vars_dict = {}
vars_dict['M'] = M
vars_dict['P'] = P
vars_dict['N'] = N
vars_dict['num_datasets'] = num_datasets
vars_dict['beta_vec'] = beta_vec
vars_dict['K'] = K
vars_dict['curr_vars'] = curr_vars
vars_dict['curr_means'] = curr_means
vars_dict['G'] = G
vars_dict['g_mc'] = g_mc

"""
# DEBUG
beta = 1
idx = 0
g  = G[idx:idx+1,:]
# Random starting point
u0, l0, s0, CVaR_surr_start = my_lib.random_init_with_local_search(g,M,beta,P)
u0 = torch.from_numpy(u0)
l0 = torch.from_numpy(l0)
u0.requires_grad = True
l0.requires_grad = True
# Optimize surrogate empirical cvar
tmp1, u_cvar, l_cvar, s_cvar = my_lib.base_learner(u0,l0,g,beta,P,1,1)
CVaR = my_lib.get_empirical_CVaR(s_cvar, l_cvar, g_mc, beta, P)
# Optimize expected rate
tmp2, u_expected, l_expected, s_expected = my_lib.base_learner(u0,l0,g,1,P,1,1)
expected = my_lib.get_empirical_CVaR(s_expected, l_expected, g_mc, beta, P)
# Optimize outage rate
tmp3, u_outage, l_outage, s_outage = my_lib.base_learner_obj_outage(u0,l0,g,beta,P,1,1)
outage = my_lib.get_empirical_CVaR(s_outage, l_outage, g_mc, beta, P)
"""

# Wrapper function to parallel
def wrapper_function(idx,beta):
    # data for current scenario
    g  = G[idx:idx+1,:]
    
    # Random starting point
    u0, l0, s0, CVaR_surr_start = my_lib.random_init_with_local_search(g,M,beta,P)
    u0 = torch.from_numpy(u0)
    l0 = torch.from_numpy(l0)
    u0.requires_grad = True
    l0.requires_grad = True
    
    # Optimize surrogate empirical cvar
    tmp1, u_cvar, l_cvar, s_cvar = my_lib.base_learner(u0,l0,g,beta,P,flag_print_stats,flag_print_graph)
    CVaR = my_lib.get_empirical_CVaR(s_cvar, l_cvar, g_mc, beta, P)
    
    # Optimize expected rate
    tmp2, u_expected, l_expected, s_expected = my_lib.base_learner(u0,l0,g,1,P,flag_print_stats,flag_print_graph)
    expected = my_lib.get_empirical_CVaR(s_expected, l_expected, g_mc, beta, P)
    
    # Optimize outage rate
    tmp3, u_outage, l_outage, s_outage = my_lib.base_learner_obj_outage(u0,l0,g,beta,P,flag_print_stats,flag_print_graph)
    outage = my_lib.get_empirical_CVaR(s_outage, l_outage, g_mc, beta, P)
    
    # returns
    return [CVaR, expected, outage]

# Prallel computation
def parallel_fun(beta):
    my_pool = Pool() # (4)
    
    # Generate input for wrapper function
    wrapper_input = [(n, beta) for n in range(num_datasets)]
    
    # Buffers to hold results
    CVaR_array = np.empty(num_datasets)
    expected_array = np.empty(num_datasets)
    outage_array = np.empty(num_datasets)
    
    # Pallel loop
    output_list = my_pool.starmap(wrapper_function, wrapper_input)
    
    # Rearange outputs
    for idx in range(num_datasets):
        CVaR_array[idx] = output_list[idx][0]
        expected_array[idx] = output_list[idx][1]
        outage_array[idx] = output_list[idx][2]
        
    return CVaR_array, expected_array, outage_array

if __name__ == '__main__':
    CVaR_array = np.zeros((num_datasets,num_betas))
    expected_array = np.zeros((num_datasets,num_betas))
    outage_array = np.zeros((num_datasets,num_betas))
    for idx_beta, beta in enumerate(beta_vec):
        print(f'{idx_beta+1}/{num_betas}: beta = {beta}')
        t = time.time()
        CVaR_array[:,idx_beta], expected_array[:,idx_beta], outage_array[:,idx_beta] = parallel_fun(beta)
        elapsed = time.time() - t
        print(f'Total runtime = {elapsed:.2f} sec')
    
        # Average over all datasets (and exclude NaNs that can appear for extremely small datasets due to high and unlikely channel gains)
        CVaR_mean = np.nanmean(CVaR_array,0)
        expected_mean = np.nanmean(expected_array,0)
        outage_mean = np.nanmean(outage_array,0)
        print(f'CVaR_mean = {CVaR_mean[idx_beta]:.4f}')
        print(f'expected_mean = {expected_mean[idx_beta]:.4f}')
        print(f'outage_mean = {outage_mean[idx_beta]:.4f}')
        
        # Add to variables dictionary
        vars_dict['CVaR_array'] = CVaR_array
        vars_dict['expected_array'] = expected_array
        vars_dict['outage_array'] = outage_array
        vars_dict['CVaR_mean'] = CVaR_mean
        vars_dict['expected_mean'] = expected_mean
        vars_dict['outage_mean'] = outage_mean
    
        # Save variables of interest
        print('Saving workspace')
        save_workspace(pickle_filename, vars_dict)
        print('')
            
    print('Done')